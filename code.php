<?php

class Building{

	public $name;
	public $floor;
	public $address;

	public function __construct($name, $floor, $address) {

		$this->name = $name;
		$this->floor = $floor;
		$this->address = $address;
	}
	
	public function getName() {
	
		return $this->name;
	}

	public function setName($name) {
		$this->name = $name;
	}
	
}

class Condominium extends Building{
	public function getName() {
	
		return $this->name;
	}

	public function setName($name) {
		$this->name = $name;
	}
}

$building = new Building('Caswynn Building', 8, 'Timog Ave., Quezon City, Philippines');

$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');
