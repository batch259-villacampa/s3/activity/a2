<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Activity 02</title>
</head>
<body>
	<h1>Activity 02</h1>

	<h2>Building</h2>
	<p>The name of the building is <?php echo $building->name; ?></p>
	<p>The <?= $building->name ?> has <?= $building->floor ?> floors.</p>
	<p>The <?= $building->name ?> is located at <?= $building->address ?>.</p>

	<?php $building->setName('Caswynn Complex');?>

	<p>The name of the building has been changed to <?= $building->getName(); ?>.</p>

	<h2>Building</h2>
	<p>The name of the building is <?php echo $condominium->name; ?></p>
	<p>The <?= $condominium->name ?> has <?= $condominium->floor ?> floors.</p>
	<p>The <?= $condominium->name ?> is located at <?= $condominium->address ?>.</p>

	<?php $condominium->setName('Enzo Tower');?>

	<p>The name of the building has been changed to <?= $condominium->getName(); ?>.</p>


</body>
</html>